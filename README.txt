{\rtf1\ansi\ansicpg1251\deff0\nouicompat\deflang1049\deflangfe1049{\fonttbl{\f0\froman\fprq2\fcharset204 Times New Roman;}{\f1\froman\fprq2\fcharset0 Times New Roman;}}
{\*\generator Riched20 10.0.19041}{\*\mmathPr\mnaryLim0\mdispDef1\mwrapIndent1440 }\viewkind4\uc1 
\pard\widctlpar\sa160\sl252\slmult1\qc\b\f0\fs28 Backend-server \'e4\'eb\'ff \'ee\'ed\'eb\'e0\'e9\'ed \'ec\'e0\'e3\'e0\'e7\'e8\'ed\'e0 \'ab\'c0\'e3\'f0\'ee\'e7\'e0\'ef\'f7\'e0\'f1\'f2\'fc\'bb\par

\pard\widctlpar\sa160\sl252\slmult1\ul\b0\'c7\'e0\'ef\'f3\'f1\'ea \'f1\'e5\'f0\'e2\'e5\'f0\'e0\ulnone  \'ef\'f0\'ee\'e8\'f1\'f5\'ee\'e4\'e8\'f2 \'f1 \'ef\'ee\'ec\'ee\'f9\'fc\'fe \'ea\'eb\'e0\'f1\'f1\'e0 \ul AgrozapchastApplication.java;\line\ulnone\'c7\'e0\'ef\'f3\'f1\'ea\'e0\'e5\'f2\'f1\'ff \'eb\'ee\'ea\'e0\'eb\'fc\'ed\'ee \'f1 \'ef\'ee\'f0\'f2\'ee\'ec 8080\par
\ul\'c4\'ee\'f1\'f2\'f3\'ef\'ed\'fb\'e5 public \'e7\'e0\'ef\'f0\'ee\'f1\'fb\ulnone :\par

\pard 
{\pntext\f0 1.\tab}{\*\pn\pnlvlbody\pnf0\pnindent0\pnstart1\pndec{\pntxta.}}
\widctlpar\fi-360\li720\sa160\sl252\slmult1 GET /categories \endash  \'f1\'ef\'e8\'f1\'ee\'ea \'e2\'f1\'e5\'f5 \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'e9\par
{\pntext\f0 2.\tab}GET /categories/\{categoryId\}/subcategories \endash  \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'ff \'f1 \'e5\'b8 \'ef\'ee\'e4\'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'ff\'ec\'e8\line path variable - categoryId\par
{\pntext\f0 3.\tab}GET /categories/\{categoryId\}/subcategories/\{subcategoryId\} \endash  \'ef\'ee\'e4\'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'ff \'f1 \'e5\'b8 \'f2\'ee\'e2\'e0\'f0\'e0\'ec\'e8\line path variable \endash  categoryId, subcategoryId\par
{\pntext\f0 4.\tab}GET /categories/\{categoryId\}/subcategories/\{subcategoryId\}/products/\{productId\} \endash  \'f1\'f2\'f0\'e0\'ed\'e8\'f6\'e0 \'f2\'ee\'e2\'e0\'f0\'e0\line path variable \endash  categoryId, subcategoryId, productId\par
{\pntext\f0 5.\tab}POST /registration \endash  \'f0\'e5\'e3\'e8\'f1\'f2\'f0\'e0\'f6\'e8\'ff \'ef\'ee\'eb\'fc\'e7\'ee\'e2\'e0\'f2\'e5\'eb\'ff\line body \endash  email, name, phone, password, password confirm\par
{\pntext\f0 6.\tab}POST /login \endash  \'e0\'e2\'f2\'ee\'f0\'e8\'e7\'e0\'f6\'e8\'ff \'ef\'ee\'eb\'fc\'e7\'ee\'e2\'e0\'f2\'e5\'eb\'ff\line body - email, password\line response - token\par

\pard\widctlpar\sa160\sl252\slmult1\par
\ul\'c4\'ee\'f1\'f2\'f3\'ef\'ed\'fb\'e5 user/admin \'e7\'e0\'ef\'f0\'ee\'f1\'fb:\par

\pard 
{\pntext\f0 1.\tab}{\*\pn\pnlvlbody\pnf0\pnindent0\pnstart1\pndec{\pntxta.}}
\widctlpar\fi-360\li720\sa160\sl252\slmult1\ulnone DELETE /logout \endash  \'e2\'fb\'f5\'ee\'e4 \'e8\'e7 \'f3\'f7\'e5\'f2\'ed\'ee\'e9 \'e7\'e0\'ef\'e8\'f1\'e8\line\f1\lang1033 header\f0\lang1049  \endash  token\par
{\pntext\f0 2.\tab}GET /user/profile \endash  \'eb\'e8\'f7\'ed\'fb\'e9 \'ea\'e0\'e1\'e8\'ed\'e5\'f2\line\f1\lang1033 header\f0\lang1049  \endash  token\par

\pard\widctlpar\li360\sa160\sl252\slmult1\ul\par

\pard\widctlpar\sa160\sl252\slmult1\'c4\'ee\'f1\'f2\'f3\'ef\'ed\'fb\'e5 admin \'e7\'e0\'ef\'f0\'ee\'f1\'fb:\f1\lang1033  \ulnone header\f0\lang1049  \f1\lang1033 - token\ul\f0\lang1049\par

\pard 
{\pntext\f0 1.\tab}{\*\pn\pnlvlbody\pnf0\pnindent0\pnstart1\pndec{\pntxta.}}
\widctlpar\fi-360\li720\sa160\sl252\slmult1\ulnone POST /categories \endash  \'e4\'ee\'e1\'e0\'e2\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line body \endash  name\ul\par
{\pntext\f0 2.\tab}\ulnone PUT /categories/\{categoryId\} \endash  \'e8\'e7\'ec\'e5\'ed\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line path variable \endash  categoryId\line body \endash  name, file(image)\ul\par
{\pntext\f0 3.\tab}\ulnone DELETE /categories/\{categoryId\} \endash  \'f3\'e4\'e0\'eb\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\ul\line\ulnone path variable \endash  categoryId\ul\par
{\pntext\f0 4.\tab}\ulnone POST /categories/\{categoryId\}/subcategories \endash  \'e4\'ee\'e1\'e0\'e2\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line path variable \endash  categoryId\line body \endash  name\ul\par
{\pntext\f0 5.\tab}\ulnone PUT /categories/\{categoryId\}/subcategories/\{subcategoryId\} \endash  \'e8\'e7\'ec\'e5\'ed\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line path variable \endash  categoryId, subcategoryId\line body \endash  name, file(image)\ul\par
{\pntext\f0 6.\tab}\ulnone DELETE /categories/\{categoryId\}/subcategories/\{subcategoryId\} \endash  \'f3\'e4\'e0\'eb\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\ul\line\ulnone path variable \endash  categoryId, subcategoryId\ul\par
{\pntext\f0 7.\tab}\ulnone POST /categories/\{categoryId\}/subcategories/\{subcategoryId\}/products \endash  \'e4\'ee\'e1\'e0\'e2\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line path variable \endash  categoryId, subcategoryId\line body \endash  name, price\ul\par
{\pntext\f0 8.\tab}\ulnone PUT /categories/\{categoryId\}/subcategories/\{subcategoryId\}/products/\{productId\} \endash  \'e8\'e7\'ec\'e5\'ed\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\line path variable \endash  categoryId, subcategoryId, productId\line body \endash  name, file(image), price\ul\par
{\pntext\f0 9.\tab}\ulnone DELETE /categories/\{categoryId\}/subcategories/\{subcategoryId\}/products/\{productId\} \endash  \'f3\'e4\'e0\'eb\'e8\'f2\'fc \'ea\'e0\'f2\'e5\'e3\'ee\'f0\'e8\'fe\ul\line\ulnone path variable \endash  categoryId, subcategoryId, productId\ul\par

\pard\widctlpar\li360\sa160\sl252\slmult1\par

\pard\widctlpar\li720\sa160\sl252\slmult1\par
}
 