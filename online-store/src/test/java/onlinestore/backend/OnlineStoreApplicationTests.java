package onlinestore.backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;

@RunWith(SpringRunner.class)
public class OnlineStoreApplicationTests {
    private String GET_CATEGORIES = "/categories";

    @Test
    public void testGetCategoriesOK() {
        when().
                get(GET_CATEGORIES).
                then().statusCode(200).body("categories", not(empty()));
    }
}