insert into role(id, name) values(1, 'ROLE_USER');
insert into role(id, name) values(2, 'ROLE_ADMIN');

insert into t_user(id, email, password, name, phone) values(1, 'user@gmail.com', '$2a$10$j2uWvQtP4FHOxToTGSSAjuxO2ndZsdIUGLzfO3iT3SR8cV9cEHEq2', 'Иванов Иван Иванович', '89352434567');
insert into t_user(id, email, password, name, phone) values(2, 'admin@gmail.com', '$2a$10$MoX3tAb749TJvxTmwKmgp.UOCPDq12kk/2RkFRS8mYpaHdTRIrBrq', 'Петров Петр Петрович', '89352532687');

insert into user_role(user_id, role_id) values(1, 1);
insert into user_role(user_id, role_id) values(2, 1);
insert into user_role(user_id, role_id) values(2, 2);