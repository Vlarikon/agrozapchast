package onlinestore.backend.service;

import onlinestore.backend.dto.request.CategoryRequest;
import onlinestore.backend.dto.response.GetCategoriesResponse;
import onlinestore.backend.dto.response.GetCategoryResponse;
import onlinestore.backend.exception.ServerException;
import org.springframework.web.multipart.MultipartFile;

public interface CategoryService {
    GetCategoriesResponse getCategories();
    void addCategory(CategoryRequest request) throws ServerException;
    void deleteCategory(Long id) throws ServerException;
    void updateCategory(Long id, CategoryRequest request, MultipartFile file) throws ServerException;
    GetCategoryResponse getCategoryById(Long id) throws ServerException;
}