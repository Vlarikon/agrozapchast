package onlinestore.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import onlinestore.backend.dto.request.ProductRequest;
import onlinestore.backend.dto.response.GetProductResponse;
import onlinestore.backend.exception.ServerErrors;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.model.Product;
import onlinestore.backend.model.Subcategory;
import onlinestore.backend.repository.ProductRepository;
import onlinestore.backend.repository.SubcategoryRepository;
import onlinestore.backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private FileCloudService fileCloudService;

    @Override
    public void addProduct(Long subcategoryId, ProductRequest request) throws ServerException {
        if(!subcategoryRepository.existsById(subcategoryId)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        if(productRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.PRODUCT_ALREADY_EXISTS);
        }
        Subcategory subcategory = subcategoryRepository.getById(subcategoryId);
        productRepository.save(new Product(request.getName(), subcategory, request.getPrice(), 0, null));
    }

    @Override
    public void deleteProduct(Long subcategoryId, Long id) throws ServerException {
        if(!subcategoryRepository.existsById(subcategoryId)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        if(productRepository.existsById(id)) {
            productRepository.deleteById(id);
        }
        throw new ServerException(ServerErrors.PRODUCT_NOT_EXISTS);
    }

    @Override
    public void updateProduct(Long subcategoryId, Long id, ProductRequest request, MultipartFile file) throws ServerException {
        if(!subcategoryRepository.existsById(subcategoryId)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        if(!productRepository.existsById(id)) {
            throw new ServerException(ServerErrors.PRODUCT_NOT_EXISTS);
        }
        if(request != null && productRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.PRODUCT_ALREADY_EXISTS);
        }
        Product productInDB = productRepository.findById(id).get();
        if(!request.getName().isEmpty()) {
            productInDB.setName(request.getName());
        }
        if(request.getPrice() != null) {
            productInDB.setPrice(request.getPrice());
        }
        if(file != null) {
            String imgUrl = fileCloudService.saveImageAndGetLink(file);
            productInDB.setImgURL(imgUrl);
        }
        productRepository.save(productInDB);
    }

    @Override
    public GetProductResponse getProductById(Long subcategoryId, Long id) throws ServerException {
        if(!subcategoryRepository.existsById(subcategoryId)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        if(!productRepository.existsById(id)) {
            throw new ServerException(ServerErrors.PRODUCT_NOT_EXISTS);
        }
        Subcategory subcategory = subcategoryRepository.findById(id).get();
        Product product = productRepository.findById(id).get();
        GetProductResponse response = new GetProductResponse();
        response.setSubcategory(subcategory);
        response.setProduct(product);
        return response;
    }
}