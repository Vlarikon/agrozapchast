package onlinestore.backend.service.impl;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class FileCloudService {

    private List<String> imageType = new ArrayList<>();

    public FileCloudService() {
        Collections.addAll(imageType, "jpg", "jpeg", "png", "bmp");
    }

    public String saveImageAndGetLink(MultipartFile file) {
        return null;
    }

    public List<String> getImageType() {
        return imageType;
    }
//    public String saveImageAndGetLink(MultipartFile file) {
//        String filename = file.getOriginalFilename();
//        if(filename == null || !imageType.contains(filename.substring(filename.lastIndexOf(".") + 1))) {
//            return null;
//        }
//        try (InputStream in = file.getInputStream()) {
//            DbxRequestConfig config = new DbxRequestConfig(folder);
//            DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
//            DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
//            String accessToken = webAuth.finish(webAuth.start()).getAccessToken();
//
//            DbxClientV2 client = new DbxClientV2(config, accessToken);
//            client.files().uploadBuilder(filename).uploadAndFinish(in);
//            SharedLinkMetadata sharedLinkMetadata = client.sharing().createSharedLinkWithSettings(filename);
//            return sharedLinkMetadata.getUrl().replace("dl=0", "raw=1");
//        } catch (IOException | DbxException ex) {
//            throw new ServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex);
//        }
//    }



}