package onlinestore.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import onlinestore.backend.dto.request.SubcategoryRequest;
import onlinestore.backend.dto.response.GetSubcategoryResponse;
import onlinestore.backend.exception.ServerErrors;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.model.Category;
import onlinestore.backend.model.Product;
import onlinestore.backend.model.Subcategory;
import onlinestore.backend.repository.CategoryRepository;
import onlinestore.backend.repository.ProductRepository;
import onlinestore.backend.repository.SubcategoryRepository;
import onlinestore.backend.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Service
public class SubcategoryServiceImpl implements SubcategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FileCloudService fileCloudService;

    @Override
    public void addSubcategory(Long categoryId, SubcategoryRequest request) throws ServerException {
        if(!categoryRepository.existsById(categoryId)) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        if(categoryRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.SUBCATEGORY_ALREADY_EXISTS);
        }
        Category category = categoryRepository.getById(categoryId);
        subcategoryRepository.save(new Subcategory(request.getName(), category, null));
    }

    @Override
    public void deleteSubcategory(Long categoryId, Long id) throws ServerException {
        if(!categoryRepository.existsById(categoryId)) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        if(subcategoryRepository.existsById(id)) {
            subcategoryRepository.deleteById(id);
        }
        throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
    }

    @Override
    public void updateSubcategory(Long categoryId, Long id, SubcategoryRequest request, MultipartFile file) throws ServerException {
        if(!categoryRepository.existsById(categoryId)) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        if(!subcategoryRepository.existsById(id)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        if(request != null && subcategoryRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.SUBCATEGORY_ALREADY_EXISTS);
        }
        Subcategory subcategoryInDB = subcategoryRepository.findById(id).get();
        if(!request.getName().isEmpty()) {
            subcategoryInDB.setName(request.getName());
        }
        if(file != null) {
            String imgUrl = fileCloudService.saveImageAndGetLink(file);
            subcategoryInDB.setImgURL(imgUrl);
        }
        subcategoryRepository.save(subcategoryInDB);
    }

    @Override
    public GetSubcategoryResponse getSubcategoryById(Long categoryId, Long id) throws ServerException {
        if(!categoryRepository.existsById(categoryId)) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        if(!subcategoryRepository.existsById(id)) {
            throw new ServerException(ServerErrors.SUBCATEGORY_NOT_EXISTS);
        }
        Subcategory subcategory = subcategoryRepository.findById(id).get();
        List<Product> products = productRepository.findAllBySubcategoryId(id);
        GetSubcategoryResponse response = new GetSubcategoryResponse();
        response.setSubcategory(subcategory);
        for(Product product : products) {
            response.addProduct(product);
        }
        return response;
    }
}