package onlinestore.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import onlinestore.backend.dto.request.LoginUserRequest;
import onlinestore.backend.dto.request.RegisterUserRequest;
import onlinestore.backend.dto.response.GetUserResponse;
import onlinestore.backend.dto.response.LoginResponse;
import onlinestore.backend.dto.response.Response;
import onlinestore.backend.exception.ServerErrors;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.model.Role;
import onlinestore.backend.model.Roles;
import onlinestore.backend.model.Session;
import onlinestore.backend.model.User;
import onlinestore.backend.repository.RoleRepository;
import onlinestore.backend.repository.SessionRepository;
import onlinestore.backend.repository.UserRepository;
import onlinestore.backend.security.jwt.JwtUtils;
import onlinestore.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtils jwtUtils;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    @Override
    public ResponseEntity registerUser(RegisterUserRequest request) throws ServerException {
        if(userRepository.findByEmail(request.getEmail()) != null) {
            throw new ServerException(ServerErrors.USER_ALREADY_EXISTS);
        }
        if(!request.getPassword().equals(request.getPasswordConfirm())) {
            throw new ServerException(ServerErrors.PASSWORD_CONFIRMATION_ERROR);
        }
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(Roles.ROLE_USER));
        User user = new User(request.getEmail(), passwordEncoder.encode(request.getPassword()),
                request.getName(), request.getPhone(), roles);
        userRepository.save(user);
        return new ResponseEntity(new Response("Вы успешно зарегистрировались"), HttpStatus.OK);
    }

    @Override
    public ResponseEntity loginUser(LoginUserRequest request) throws ServerException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token;
        User user = (User) authentication.getPrincipal();
        if(sessionRepository.existsByUserId(user.getId())) {
            Session session = sessionRepository.findByUserId(user.getId());
            token = session.getToken();
        }
        else {
            token = jwtUtils.generateJwtToken(authentication);
            sessionRepository.save(new Session(user, token));
        }
        return new ResponseEntity(new LoginResponse(token), HttpStatus.OK);
    }

    @Override
    public ResponseEntity logOutUser(String token) throws ServerException {
        if(token == null || token.isEmpty()) {
            return new ResponseEntity(new Response("Вы не авторизованы"), HttpStatus.UNAUTHORIZED);
        }
        Session session = sessionRepository.findByToken(token);
        if(session == null) {
            return new ResponseEntity(new Response("Недействительный токен"), HttpStatus.BAD_REQUEST);
        }
        sessionRepository.delete(session);
        return new ResponseEntity(new Response(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity getUser(String token) throws ServerException {
        if(token == null || token.isEmpty()) {
            return new ResponseEntity(new Response("Вы не авторизованы"), HttpStatus.UNAUTHORIZED);
        }
        Session session = sessionRepository.findByToken(token);
        if(session == null) {
            return new ResponseEntity(new Response("Недействительный токен"), HttpStatus.BAD_REQUEST);
        }
        User user = sessionRepository.findByToken(token).getUser();
        if(user != null && user.getRoles().contains(roleRepository.getById(1L))) {
            return new ResponseEntity(new GetUserResponse(
                    user.getName(), user.getUsername(), user.getPhone(), user.getOrders()),
                    HttpStatus.OK);
        }
        return new ResponseEntity(new Response("Вы не авторизованы"), HttpStatus.FORBIDDEN);
    }
}