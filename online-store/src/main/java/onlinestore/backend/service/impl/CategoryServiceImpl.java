package onlinestore.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import onlinestore.backend.dto.request.CategoryRequest;
import onlinestore.backend.dto.response.GetCategoriesResponse;
import onlinestore.backend.dto.response.GetCategoryResponse;
import onlinestore.backend.exception.ServerErrors;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.model.Category;
import onlinestore.backend.model.Subcategory;
import onlinestore.backend.repository.CategoryRepository;
import onlinestore.backend.repository.SubcategoryRepository;
import onlinestore.backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private FileCloudService fileCloudService;

    @Override
    public GetCategoriesResponse getCategories() {
        List<Category> categories = categoryRepository.findAll();
        GetCategoriesResponse response = new GetCategoriesResponse();
        for(Category category : categories) {
            response.addCategory(category);
        }
        return response;
    }

    @Override
    public void addCategory(CategoryRequest request) throws ServerException {
        if(categoryRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.CATEGORY_ALREADY_EXISTS);
        }
        categoryRepository.save(new Category(request.getName(), null));
    }

    @Override
    public void deleteCategory(Long id) throws ServerException {
        if(categoryRepository.existsById(id)) {
            categoryRepository.deleteById(id);
        }
        throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
    }

    @Override
    public void updateCategory(Long id, CategoryRequest request, MultipartFile file) throws ServerException {
        if(!categoryRepository.existsById(id)) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        if(request != null && categoryRepository.findByName(request.getName()) != null) {
            throw new ServerException(ServerErrors.CATEGORY_ALREADY_EXISTS);
        }
        Category categoryInDB = categoryRepository.findById(id).get();
        if(!request.getName().isEmpty()) {
            categoryInDB.setName(request.getName());
        }
        if(file != null) {
            String imgUrl = fileCloudService.saveImageAndGetLink(file);
            categoryInDB.setImgURL(imgUrl);
        }
        categoryRepository.save(categoryInDB);
    }

    @Override
    public GetCategoryResponse getCategoryById(Long id) throws ServerException {
        Optional<Category> category = categoryRepository.findById(id);
        if(category.isEmpty()) {
            throw new ServerException(ServerErrors.CATEGORY_NOT_EXISTS);
        }
        List<Subcategory> subcategories = subcategoryRepository.findAllByCategoryId(id);
        GetCategoryResponse response = new GetCategoryResponse();
        response.setCategory(category.get());
        for(Subcategory subcategory : subcategories) {
            response.addSubcategory(subcategory);
        }
        return response;
    }
}