package onlinestore.backend.service;

import onlinestore.backend.dto.request.ProductRequest;
import onlinestore.backend.dto.response.GetProductResponse;
import onlinestore.backend.exception.ServerException;
import org.springframework.web.multipart.MultipartFile;

public interface ProductService {
    void addProduct(Long subcategoryId, ProductRequest request) throws ServerException;
    void deleteProduct(Long subcategoryId, Long id) throws ServerException;
    void updateProduct(Long subcategoryId, Long id, ProductRequest request, MultipartFile file) throws ServerException;
    GetProductResponse getProductById(Long subcategoryId, Long id) throws ServerException;
}