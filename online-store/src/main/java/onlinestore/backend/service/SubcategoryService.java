package onlinestore.backend.service;

import onlinestore.backend.dto.request.SubcategoryRequest;
import onlinestore.backend.dto.response.GetSubcategoryResponse;
import onlinestore.backend.exception.ServerException;
import org.springframework.web.multipart.MultipartFile;

public interface SubcategoryService {
    void addSubcategory(Long categoryId, SubcategoryRequest request) throws ServerException;
    void deleteSubcategory(Long categoryId, Long id) throws ServerException;
    void updateSubcategory(Long categoryId, Long id, SubcategoryRequest request, MultipartFile file) throws ServerException;
    GetSubcategoryResponse getSubcategoryById(Long categoryId, Long id) throws ServerException;
}