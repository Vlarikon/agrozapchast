package onlinestore.backend.service;

import onlinestore.backend.dto.request.LoginUserRequest;
import onlinestore.backend.dto.request.RegisterUserRequest;
import onlinestore.backend.exception.ServerException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
   ResponseEntity registerUser(RegisterUserRequest request) throws ServerException;

   ResponseEntity loginUser(LoginUserRequest request) throws ServerException;

   ResponseEntity logOutUser(String token) throws ServerException;

   ResponseEntity getUser(String token) throws ServerException;
}