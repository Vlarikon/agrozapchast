package onlinestore.backend.repository;

import onlinestore.backend.model.Session;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends CrudRepository<Session, Long> {
    Session findByToken(String token);
    boolean existsByUserId(Long id);
    Session findByUserId(Long id);
}