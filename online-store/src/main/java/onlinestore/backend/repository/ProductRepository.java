package onlinestore.backend.repository;

import onlinestore.backend.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllBySubcategoryId(Long id);
    Product findByName(String name);
}