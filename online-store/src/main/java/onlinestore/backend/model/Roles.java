package onlinestore.backend.model;

import lombok.Getter;

@Getter
public enum Roles {
    ROLE_ADMIN,
    ROLE_USER;
}