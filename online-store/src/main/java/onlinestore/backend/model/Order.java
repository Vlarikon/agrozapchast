package onlinestore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user_order")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "created_at")
    private Date createdAt;

    @Column
    private String status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    private Set<ProductInOrder> products;

    public Order(User user, String status) {
        this.user = user;
        this.status = status;
        createdAt = new Date();
    }
}