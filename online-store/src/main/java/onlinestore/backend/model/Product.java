package onlinestore.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true, length = 150)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "subcategory_id")
    private Subcategory subcategory;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer count;

    @Column(name = "img_url", columnDefinition = "TEXT")
    private String imgURL;

    public Product(String name, Subcategory subcategory, double price, int count, String imgURL) {
        this.name = name;
        this.subcategory = subcategory;
        this.price = price;
        this.count = count;
        this.imgURL = imgURL;
    }
}