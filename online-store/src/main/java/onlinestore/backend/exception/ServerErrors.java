package onlinestore.backend.exception;

public enum ServerErrors {
    CATEGORY_ALREADY_EXISTS("Категория с этим именем уже существует"),
    CATEGORY_NOT_EXISTS("Категории не существует"),

    SUBCATEGORY_ALREADY_EXISTS("Подкатегория с этим именем уже существует"),
    SUBCATEGORY_NOT_EXISTS("Подкаегории не существует"),

    PRODUCT_ALREADY_EXISTS("Товар с этим именем уже существует"),
    PRODUCT_NOT_EXISTS("Товара не существует"),

    USER_ALREADY_EXISTS("Пользователь с такой почтой уже зарегистрирован"),
    PASSWORD_CONFIRMATION_ERROR("Пароли не совпадают"),
    LOGIN_ERROR("Введен неверный пароль или почта"),
    USER_ACCESS_FORBIDDEN("Доступ запрещен"),

    INVALID_TOKEN("Недействительный токен"),
    INVALID_FILE_FORMAT("Формат файла должен быть: jpg, jpeg, png или bmp")
    ;

    private final String message;

    ServerErrors(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}