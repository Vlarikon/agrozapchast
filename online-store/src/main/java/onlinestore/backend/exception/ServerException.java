package onlinestore.backend.exception;

public class ServerException extends Exception {
    private ServerErrors serverError;

    public ServerException(ServerErrors serverError) {
        this.serverError = serverError;
    }

    public ServerErrors getServerError() {
        return serverError;
    }
}