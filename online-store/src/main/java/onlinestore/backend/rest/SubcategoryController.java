package onlinestore.backend.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import onlinestore.backend.dto.request.SubcategoryRequest;
import onlinestore.backend.dto.response.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/categories/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
public interface SubcategoryController {

    @PostMapping(value = "/subcategories", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Add Subcategory", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "A new Subcategory has been added successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Subcategory name isn't unique"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "File isn't image"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Category doesn't exist")
    })
    ResponseEntity addSubcategory(@RequestHeader("token") String token, @PathVariable("categoryId") Long categoryId,
            @RequestBody @Valid SubcategoryRequest request);

    @DeleteMapping(value = "/subcategories/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete Subcategory by id", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Subcategory is deleted successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Subcategory doesn't exist")
    })
    ResponseEntity deleteSubcategory(@RequestHeader("token") String token, @PathVariable("categoryId") Long categoryId,
                                                                              @PathVariable("id") Long id);

    @PutMapping(value = "/subcategories/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Update Subcategory by id(New image or name)", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Subcategory updated successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Subcategory doesn't exist"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "File isn't image")
    })
    ResponseEntity updateSubcategory(@RequestHeader("token") String token, @PathVariable("categoryId") Long categoryId,
            @PathVariable("id") Long id, @RequestBody(required = false) @Valid SubcategoryRequest request,
            @RequestPart(name = "file", required = false) MultipartFile file);

    @GetMapping(value = "/subcategories/{id}")
    @ApiOperation(value = "Return Subcategory by id", notes = "This is an public API", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Subcategory doesn't exist")
    })
    ResponseEntity getSubcategory(@PathVariable("categoryId") Long categoryId, @PathVariable("id") Long id);
}