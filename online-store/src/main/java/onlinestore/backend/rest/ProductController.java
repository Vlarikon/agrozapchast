package onlinestore.backend.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import onlinestore.backend.dto.request.ProductRequest;
import onlinestore.backend.dto.response.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/subcategories/{subcategoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProductController {

    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Add Product", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "A new Product has been added successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Product name isn't unique"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "File isn't image"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Subcategory doesn't exist")
    })
    ResponseEntity addProduct(@RequestHeader("token") String token, @PathVariable("subcategoryId") Long subcategoryId,
                                                                       @RequestBody @Valid ProductRequest request);

    @DeleteMapping(value = "/products/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete Product by id", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Product is deleted successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Product doesn't exist")
    })
    ResponseEntity deleteProduct(@RequestHeader("token") String token, @PathVariable("subcategoryId") Long subcategoryId,
                                                                          @PathVariable("id") Long id);

    @PutMapping(value = "/products/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Update Product by id(New image or name)", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Product updated successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Product doesn't exist"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "File isn't image")
    })
    ResponseEntity updateProduct(@RequestHeader("token") String token, @PathVariable("subcategoryId") Long subcategoryId,
            @PathVariable("id") Long id, @RequestBody(required = false) ProductRequest request,
            @RequestPart(name = "file", required = false) MultipartFile file);

    @GetMapping(value = "/products/{id}")
    @ApiOperation(value = "Return Product by id", notes = "This is an public API", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Product doesn't exist")
    })
    ResponseEntity getProduct(@PathVariable("subcategoryId") Long subcategoryId, @PathVariable("id") Long id);
}