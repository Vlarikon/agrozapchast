package onlinestore.backend.rest.impl;

import onlinestore.backend.dto.request.CategoryRequest;
import onlinestore.backend.dto.response.Response;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.rest.CategoryController;
import onlinestore.backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class CategoryControllerImpl implements CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Override
    public ResponseEntity getCategories() {
        return new ResponseEntity(categoryService.getCategories(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity addCategory(String token, CategoryRequest request) {
        //if(token validation) {
        try {
            categoryService.addCategory(request);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
        //}
        //return new ResponseEntity(new Response("No access"), HttpStatus.FORBIDDEN);
    }

    @Override
    public ResponseEntity deleteCategory(String token, Long id) {
        //if(token validation) {
        try {
            categoryService.deleteCategory(id);
            return new ResponseEntity(new Response(), HttpStatus.NO_CONTENT);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
        //}
        //return new ResponseEntity(new Response("No access"), HttpStatus.FORBIDDEN);
    }

    @Override
    public ResponseEntity updateCategory(String token, Long id, CategoryRequest request, MultipartFile file) {
        //if(token validation) {
        try {
            categoryService.updateCategory(id, request, file);
            return new ResponseEntity(new Response(), HttpStatus.NO_CONTENT);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
        //}
        //return new ResponseEntity(new Response("No access"), HttpStatus.FORBIDDEN);
    }

    @Override
    public ResponseEntity getCategory(Long id) {
        try {
            return new ResponseEntity(categoryService.getCategoryById(id), HttpStatus.OK);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}