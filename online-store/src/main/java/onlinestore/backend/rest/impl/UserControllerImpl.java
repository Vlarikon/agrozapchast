package onlinestore.backend.rest.impl;

import onlinestore.backend.dto.request.LoginUserRequest;
import onlinestore.backend.dto.request.RegisterUserRequest;
import onlinestore.backend.dto.response.Response;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.rest.UserController;
import onlinestore.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity registerUser(RegisterUserRequest request) {
        try {
            userService.registerUser(request);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity logInUser(LoginUserRequest request) {
        try {
            return new ResponseEntity(userService.loginUser(request), HttpStatus.OK);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity logOutUser(String token) {
        try {
            return new ResponseEntity(userService.logOutUser(token), HttpStatus.OK);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity getUser(String token) {
        try {
            return new ResponseEntity(userService.getUser(token), HttpStatus.OK);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}