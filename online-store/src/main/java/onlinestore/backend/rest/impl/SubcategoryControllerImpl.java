package onlinestore.backend.rest.impl;

import onlinestore.backend.dto.request.SubcategoryRequest;
import onlinestore.backend.dto.response.Response;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.rest.SubcategoryController;
import onlinestore.backend.service.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class SubcategoryControllerImpl implements SubcategoryController {

    @Autowired
    private SubcategoryService subcategoryService;

    @Override
    public ResponseEntity addSubcategory(String token, Long categoryId, SubcategoryRequest request) {
        try {
            subcategoryService.addSubcategory(categoryId, request);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity deleteSubcategory(String token, Long categoryId, Long id) {
        try {
            subcategoryService.deleteSubcategory(categoryId, id);
            return new ResponseEntity(new Response(), HttpStatus.NO_CONTENT);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity updateSubcategory(String token, Long categoryId, Long id, SubcategoryRequest request, MultipartFile file) {
        try {
            subcategoryService.updateSubcategory(categoryId, id, request, file);
            return new ResponseEntity(new Response(), HttpStatus.NO_CONTENT);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity getSubcategory(Long categoryId, Long id) {
        try {
            return new ResponseEntity(subcategoryService.getSubcategoryById(categoryId, id), HttpStatus.OK);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}