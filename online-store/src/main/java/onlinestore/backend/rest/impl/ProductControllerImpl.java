package onlinestore.backend.rest.impl;

import onlinestore.backend.dto.request.ProductRequest;
import onlinestore.backend.dto.response.Response;
import onlinestore.backend.exception.ServerException;
import onlinestore.backend.rest.ProductController;
import onlinestore.backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ProductControllerImpl implements ProductController {

    @Autowired
    private ProductService productService;

    @Override
    public ResponseEntity addProduct(String token, Long subcategoryId, ProductRequest request) {
        try {
            productService.addProduct(subcategoryId, request);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity deleteProduct(String token, Long subcategoryId, Long id) {
        try {
            productService.deleteProduct(subcategoryId, id);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity updateProduct(String token, Long subcategoryId, Long id, ProductRequest request, MultipartFile file) {
        try {
            productService.updateProduct(subcategoryId, id, request, file);
            return new ResponseEntity(new Response(), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity getProduct(Long subcategoryId, Long id) {
        try {
            return new ResponseEntity(productService.getProductById(subcategoryId, id), HttpStatus.CREATED);
        } catch (ServerException ex) {
            return new ResponseEntity(new Response(ex.getServerError().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}