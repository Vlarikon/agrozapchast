package onlinestore.backend.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import onlinestore.backend.dto.request.CategoryRequest;
import onlinestore.backend.dto.response.GetCategoriesResponse;
import onlinestore.backend.dto.response.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public interface CategoryController {

    @GetMapping(value = "/categories")
    @ApiOperation(value = "Return all categories", notes = "This is a public API", response = GetCategoriesResponse.class)
    @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success")
    ResponseEntity getCategories();

    @PostMapping(value = "/categories", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Add category", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "A new category has been added successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Category name isn't unique")
    })
    ResponseEntity addCategory(@RequestHeader("token") String token, @RequestBody @Valid CategoryRequest request);

    @DeleteMapping(value = "/categories/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete category by id", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Category is deleted successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Category doesn't exist")
    })
    ResponseEntity deleteCategory(@RequestHeader("token") String token, @PathVariable("id") Long id);

    @PutMapping(value = "/categories/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Update category by id(New image or name)", notes = "This is an admin API", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Category updated successfully"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Category doesn't exist")
    })
    ResponseEntity updateCategory(@RequestHeader("token") String token,
            @PathVariable("id") Long id, @RequestBody(required = false) @Valid CategoryRequest request,
            @RequestParam(name = "file", required = false) MultipartFile file);

    @GetMapping(value = "/categories/{id}")
    @ApiOperation(value = "Return category by id", notes = "This is an public API", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Category doesn't exist")
    })
    ResponseEntity getCategory(@PathVariable("id") Long id);
}