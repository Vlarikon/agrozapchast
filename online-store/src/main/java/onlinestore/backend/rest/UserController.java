package onlinestore.backend.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import onlinestore.backend.dto.request.LoginUserRequest;
import onlinestore.backend.dto.request.RegisterUserRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserController {

    @PostMapping(value = "/registration",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value="Register user")
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid body")
    })
    ResponseEntity registerUser(@RequestBody @Valid RegisterUserRequest request);

    @PostMapping(value = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value="Log in user")
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid username/password")
    })
    ResponseEntity logInUser(@RequestBody LoginUserRequest request);

    @DeleteMapping(value = "/logout")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value="Log out user")
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid token")
    })
    ResponseEntity logOutUser(@RequestHeader String token);

    @GetMapping(value = "/user/profile")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value="Get user")
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Success"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid token")
    })
    ResponseEntity getUser(@RequestHeader String token);
}