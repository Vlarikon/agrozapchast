package onlinestore.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import onlinestore.backend.model.Order;

import java.util.Set;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class GetUserResponse {
    private String name;
    private String email;
    private String phone;
    private Set<Order> orders;
}