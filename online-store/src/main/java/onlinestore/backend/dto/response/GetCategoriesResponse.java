package onlinestore.backend.dto.response;


import lombok.Data;
import lombok.experimental.Accessors;
import onlinestore.backend.model.Category;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class GetCategoriesResponse {
    private List<Category> categories = new ArrayList<>();

    public void addCategory(Category category) {
        categories.add(category);
    }
}