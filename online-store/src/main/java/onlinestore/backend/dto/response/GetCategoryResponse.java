package onlinestore.backend.dto.response;

import lombok.Data;
import lombok.experimental.Accessors;
import onlinestore.backend.model.Category;
import onlinestore.backend.model.Subcategory;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class GetCategoryResponse {
    private Category category;
    private List<Subcategory> subcategories = new ArrayList<>();

    public void addSubcategory(Subcategory subcategory) {
        subcategories.add(subcategory);
    }
}