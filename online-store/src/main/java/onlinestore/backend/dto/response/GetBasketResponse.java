package onlinestore.backend.dto.response;

import lombok.Data;
import onlinestore.backend.model.Basket;

@Data
public class GetBasketResponse {
    private Basket basket;
}