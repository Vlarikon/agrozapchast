package onlinestore.backend.dto.response;

import lombok.Data;
import onlinestore.backend.model.Product;
import onlinestore.backend.model.Subcategory;

@Data
public class GetProductResponse {
    private Subcategory subcategory;
    private Product product;
}