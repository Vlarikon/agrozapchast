package onlinestore.backend.dto.response;

import lombok.Data;
import lombok.experimental.Accessors;
import onlinestore.backend.model.Product;
import onlinestore.backend.model.Subcategory;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class GetSubcategoryResponse {
    private Subcategory subcategory;
    private List<Product> products = new ArrayList<>();

    public void addProduct(Product product) {
        products.add(product);
    }
}