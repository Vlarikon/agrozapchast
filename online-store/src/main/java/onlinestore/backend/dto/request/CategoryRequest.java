package onlinestore.backend.dto.request;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class CategoryRequest {
    @Size(min = 3, message = "Слишком короткое название")
    private String name;
}