package onlinestore.backend.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class RegisterUserRequest {
    @Email(message = "Некорректный формат почты")
    private String email;
    @Size(min = 4, max = 20, message = "Пароль должен быть не меньше 4 и не больше 20 символов")
    private String password;
    private String passwordConfirm;
    @Size(min = 4, message = "Слишком короткое имя")
    private String name;
    @Size(max = 11)
    @Pattern(regexp="8(^$|[0-9]{10})", message = "Некорректный ввод")
    private String phone;
}