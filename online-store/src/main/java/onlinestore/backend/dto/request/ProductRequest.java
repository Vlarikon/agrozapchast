package onlinestore.backend.dto.request;

import lombok.Data;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
public class ProductRequest {
    private Long subcategoryId;
    @Size(min=3, message = "Слишком короткое название")
    private String name;
    @Positive(message = "Некорректная цена")
    private Double price;
}